
'''
author: Maninder Singh Jheeta

This file parse any .csv formatted data and returns a dictionary representation in the form of
[ Attribute 1 : List of values,  Attribute 2 : List of values]

'''

import os
import pandas as pd
import string

def parse_csv(filename):
    if filename == "":
        print("File name cannot be empty")
        exit(1)

    # Retrieve current working directory
    cwd = os.getcwd()

    pathToResourcesDir = "{}/resources/{}".format(cwd, filename)
    df = pd.read_csv(pathToResourcesDir)

    return df

def get_list(filename, fieldName):
    df = parse_csv(filename)
    diseases = df[fieldName]

    return diseases

def clean_text(text):
    text = text.translate({ord(c): " " for c in string.whitespace})
    text = text.replace('(', ' ')
    text = text.replace(')', ' ')
    text = text.replace("'", ' ')
    ''.join( c for c in text if  c not in '()' )

    return text