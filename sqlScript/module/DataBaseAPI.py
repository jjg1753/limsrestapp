'''
@author: Varun Rajiv Mantri

    Generic functions for CRUD related actions.
'''

# This package needs to be installed
import pymysql

def createTable(connection,tableName,definition):
    '''
    Generic function that creates a table if not present
    :param connection: the connection handle of the data base
    :param tableName: name of the table
    :param definition: definition of the columns
    :return: None
    '''
    with connection.cursor() as cursor:
        # Create a new record
        sql = "CREATE TABLE if not exists "+tableName+definition
        cursor.execute(sql)
        print('table created if present')


def displayTables(connection,databaseName='ams'):
    '''
    Function used to show all the tables present in the database
    :param connection: the connection handle of the data base
    :param databaseName: default (ams) but can be used with other values if required
    :return: None
    '''
    with connection.cursor() as cursor:
        sql="show tables"
        cursor.execute(sql)
        print("Following tables exist in the database ("+databaseName+")",cursor.fetchall())


def insertIntoTable(connection,tableName,columns,data):
    '''
    Generic function used for inserting values into the table
    :param connection: the connection handle of the data base
    :param columns: list containing names of the columns
    :param tableName: Name of the table
    :param data: data to be inserted  (requires data to be in dictionary format)
    :return: None
    '''
    columnHandles="("
    for index in range(len(columns)-1):
        columnHandles=columnHandles+"`"+columns[index]+"`,"
    columnHandles=columnHandles+"`"+columns[len(columns)-1]+"`)"
    with connection.cursor() as cursor:
        # Create a new record
        for row in data:
            #temp=data[key]
            placeHolder="('"+str(row[0])+"'"
            for val in row[1:]:
                placeHolder=placeHolder+",'"+str(val)+"'"
            placeHolder=placeHolder+")"
            print(columnHandles)
            print(placeHolder)
            sql = "INSERT ignore INTO `" + tableName + "`" + columnHandles + " VALUES " + placeHolder
            cursor.execute(sql)
        print('records inserted')
    connection.commit()



def printAllValues(connection,tableName):
    '''
    Used to display all the values that have been added into the database
    :param connection: the connection handle of the data base
    :param tableName: Name of the table
    :return: None
    '''
    with connection.cursor() as cursor:
        # Read a single record
        sql = "SELECT * FROM `"+tableName+"`"
        cursor.execute(sql)
        result = cursor.fetchall()
        print(result)


def dropTable(connection,tableName):
    '''
    Function used for dropping table
    :param connection: the connection handle of the data base
    :param tableName: Name of the table
    :return: None
    '''
    with connection.cursor() as cursor:
        sql="DROP TABLE `"+tableName+"`"
        cursor.execute(sql)
        print('table droppped')





def createConnectionObject():
    '''
    Creates a connection handle to the database
    :return:
    '''
    connection = pymysql.connect(host='localhost',
                                 user='root',
                                 password='jadoo',
                                 db='ams',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection