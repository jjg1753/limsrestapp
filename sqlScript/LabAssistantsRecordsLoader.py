
'''
author: Maninder Singh Jheeta

This python script is used to create Lab Assistant table, and then the table with dummy Assistants records.

'''

import module.DataBaseAPI as dbapi
import module.csvparser as csvparse
import random




TABLE_NAME = 'Assistant'
FILE_NAME = 'AsistantNames.csv'
FIELD_NAME = 'Name'

def create_assistant_table():
    connection = dbapi.createConnectionObject()

    try:
        # creating Disease table
        dbapi.createTable(connection,TABLE_NAME,"(assistant_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,name VARCHAR(500),phleb_id INT)")

        # Display all the created tables
        dbapi.displayTables(connection)
    finally:
        connection.close()

def load_assistant_table():
    create_assistant_table()
    # Inserting data into the Test table
    diseases = csvparse.get_list(FILE_NAME, FIELD_NAME)
    connection = dbapi.createConnectionObject()
    try:
        # insert each disease record into the disease table
        for firstName in diseases:
            temp=[]
            firstName = csvparse.clean_text(firstName)
            temp.append(firstName)
            temp.append(random.randint(1,100))
            data=[temp]
            dbapi.insertIntoTable(connection, TABLE_NAME, ["name","phleb_id"],data)
        dbapi.printAllValues(connection, TABLE_NAME)

    finally:
        connection.close()





load_assistant_table()