
'''
author: Maninder Singh Jheeta

This python script is used to create Diseases table, and then the table with dummy diseases records.

'''

import module.DataBaseAPI as dbapi
import module.csvparser as csvparse

TABLE_NAME = 'Disease'
FILE_NAME = 'diseases.csv'
FIELD_NAME = 'diagnose'

def create_disease_table():
    connection = dbapi.createConnectionObject()

    try:
        # creating Disease table
        dbapi.createTable(connection,TABLE_NAME,"(disease_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,name VARCHAR(500), description VARCHAR(500))")

        # Display all the created tables
        dbapi.displayTables(connection)
    finally:
        connection.close()

def load_disease_table():
    create_disease_table()
    # Inserting data into the Test table
    diseases = csvparse.get_list(FILE_NAME, FIELD_NAME)
    connection = dbapi.createConnectionObject()

    try:
        # insert each disease record into the disease table
        for disease in diseases:
            disease = csvparse.clean_text(disease)
            data=[]
            data.append([disease,disease])
            dbapi.insertIntoTable(connection, TABLE_NAME, ["name","description"],data)
        dbapi.printAllValues(connection, TABLE_NAME)
    finally:
        connection.close()

load_disease_table()