
'''
@author: Varun Rajiv Mantri

File for loading phlebotomist table and mappings table

'''

import random
import string
from module.DataBaseAPI import createConnectionObject,createTable,insertIntoTable,printAllValues,dropTable
import names
import faker


# generate about 10 different kinds of lisenses
lisenses=[]             # Used as global variable as its needed ahead in another function

def generateDataForPhlebotomist():
    '''
    Function used for generating random 16 character lisense numbers and assigning them to 100 med ids
    :return: generated data
    '''
    data=[]
    fake=faker.Faker()
    for _ in range(10):
        lisenses.append(''.join([random.choice(string.ascii_letters + string.digits) for n in range(16)]))
    # assuming there are 100 med ids
    for phleb_id in range(1,100):
        # randomly assigning lisenses to med_ids from 1 to 100
        data.append([names.get_full_name(),fake.address(),lisenses[random.randint(0,9)]])
    return data


def handlePhlebTable(connection):
    '''
    Complete handler for the phlebotomist table (creates table if not present, populates it with data if necessary)
    :param connection:
    :return: None
    '''
    # creating Liscenses table
    createTable(connection, "Phlebotomist", "(phleb_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,name VARCHAR(20), address VARCHAR(50),lisense_number VARCHAR(20))")
    data=generateDataForPhlebotomist()
    insertIntoTable(connection, "Phlebotomist", ["name","address","lisense_number"], data)



def generateDataForMapping():
    '''
    Generate data for the mapping table
    :return: generated data
    '''
    data=[]
    for lisense_number in lisenses:
        randNumber=random.randint(1,5)
        cache={}
        for _ in range(randNumber):
            temp='Not in Cache'
            while temp not in cache.keys():
                temp = random.randint(1, 11)
                if temp not in cache.keys():
                    cache[temp]=True
            data.append([lisense_number,temp])
    return data

def handleMappingTable(connection):
    '''
    Complete handler for the mapping table (creates table if not present, populates it with data if necessary)
    :return:None
    '''
    # creating the Mapping table
    createTable(connection, "Mapping", "(lisense_number VARCHAR(20),test_id INT,"
                                       "FOREIGN KEY test_id(test_id) REFERENCES Tests(test_id))")
    data=generateDataForMapping()
    insertIntoTable(connection, "Mapping", ["lisense_number", "test_id"], data)




def main():
    connection = createConnectionObject()
    try:
        handlePhlebTable(connection)
        handleMappingTable(connection)

    finally:
        connection.close()





main()