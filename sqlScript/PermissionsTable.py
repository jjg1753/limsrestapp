

'''
@author: Varun Rajiv Mantri

File for loading permissions

'''
from module.DataBaseAPI import createConnectionObject,createTable,insertIntoTable,printAllValues,dropTable


def generateDataForPermissions():
    '''
    Generate hard coded data for users
    :return: hardcoded data
    '''
    return [['patients'],['diseases'],['assistants'],['phlebotomists'],['appointments'],['tests']]




def handlePermissionsTable(connection):
    '''
    Create Tables and insert data for the users
    :param connection: connection object
    :return:
    '''
    createTable(connection, "Permissions",
                "(permission_id int not null auto_increment, task varchar(20), primary key(permission_id))")
    data = generateDataForPermissions()
    insertIntoTable(connection, "Permissions",
                    ["task"], data)



def main():
    connection = createConnectionObject()
    try:
        handlePermissionsTable(connection)
    finally:
        connection.close()


main()