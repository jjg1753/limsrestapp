

'''
@author: Jeet Gandhi


File for loading in patients data
'''



import random
from module.DataBaseAPI import createConnectionObject,createTable,insertIntoTable,printAllValues,dropTable
import names
import faker


def random_with_N_digits(n):
    range_start = 10 ** (n - 1)
    range_end = (10 ** n) - 1
    return random.randint(range_start, range_end)


def generateDataForPatients():
    """
    Function used for generating random data for patients
    table assuming the insurance number is 7 digits long
    :return: generated data
    """
    data = []
    fake = faker.Faker()
    gender = ['M', 'F']
    # assuming there are 100 med ids
    for patient_id in range(1, 100):
        # randomly assigning lisenses to med_ids from 1 to 100
        data.append([names.get_full_name(), random_with_N_digits(10), random_with_N_digits(9),
                            random_with_N_digits(2), gender[patient_id % 2], fake.address(), random_with_N_digits(7)])
    return data


def handlePatientsTable(connection):
    """
    Complete handler for the patients table (creates table if not present, populates it with data if necessary)
    :param connection:
    :return:
    """
    createTable(connection, "Patients",
                "(patient_id INT NOT NULL AUTO_INCREMENT primary key, patient_name VARCHAR(100) NOT NULL, "
                "patient_contact_number VARCHAR(20) NOT NULL, patient_SSN VARCHAR(9) NOT NULL, "
                "patient_age VARCHAR(3) NOT NULL, patient_sex VARCHAR(1) NOT NULL, patient_address VARCHAR(200), "
                "patient_insurance_number VARCHAR(20))")
    data = generateDataForPatients()
    insertIntoTable(connection, "Patients",
                    ["patient_name", "patient_contact_number", "patient_SSN", "patient_age",
                     "patient_sex", "patient_address", "patient_insurance_number"], data)




def main():
    connection = createConnectionObject()
    try:
        handlePatientsTable(connection)
    finally:
        connection.close()

main()