
'''
@author: Varun Rajiv Mantri

File for loading roles table

'''

from module.DataBaseAPI import createConnectionObject,createTable,insertIntoTable,printAllValues,dropTable


def generateDataForRoles():
    '''
    Generate hard coded data for users
    :return: hardcoded data
    '''
    return [['ASSIST',1],
            ['ASSIST', 3],
            ['ASSIST', 5],
            ['PATIE', 5],
            ['PHLEB', 1],
            ['PHLEB', 2],
            ['PHLEB', 3],
            ['PHLEB', 4],
            ['PHLEB', 5],
            ['PHLEB', 6]]

def handleRolesTable(connection):
    '''
    Create Tables and insert data for the users
    :param connection: connection object
    :return:
    '''
    createTable(connection, "Roles",
                "(role varchar(20) not null, permission_id int not null, primary key(role,permission_id))")
    data = generateDataForRoles()
    insertIntoTable(connection, "Roles",
                    ["role", "permission_id"], data)



def main():
    connection = createConnectionObject()
    try:
        handleRolesTable(connection)
    finally:
        connection.close()
main()