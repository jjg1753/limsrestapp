
'''
@author: Varun Rajiv Mantri

File for loading in Tests table
'''



from module.DataBaseAPI import createConnectionObject,createTable,insertIntoTable,printAllValues,dropTable

def generateDataForTests():
    '''
    Function used for generating data for different tests
    :return: generated data
    '''
    data=[["Blood Culture (BC)"],["PT/PT/INR"],["H&H, Hemoglobin, Hematocrit"],["Hematocrit(HCT)"],["BNP Peptide"],["(CBC) Complete Blood Count"],["Hemoglobin (Hgb)"],["Platelet Count (plt. ct)"],["White Blood Count (WBC)"],["HbgA1C"],["Erythrocyte Sedimentation Rate (ESR)"]]
    return data


def handleTestsTable(connection):
    '''
    Complete handler for the tests table (creates table if not present, populates it with data if necessary)
    :param connection: the connection handle of the data base
    :return: None
    '''
    # creating Tests table
    createTable(connection, "Tests", "(test_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,test_name VARCHAR(20))")
    # Inserting data into the Test table
    data = generateDataForTests()
    insertIntoTable(connection, "Tests", ["test_name"], data)


def main():
    connection=createConnectionObject()
    try:
        handleTestsTable(connection)
    finally:
        connection.close()


main()