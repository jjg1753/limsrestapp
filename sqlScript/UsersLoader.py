
'''
@author: Varun Rajiv Mantri

File for loading users table

'''

from module.DataBaseAPI import createConnectionObject,createTable,insertIntoTable,printAllValues,dropTable


def generateDataForUsers():
    '''
    Generate hard coded data for users
    :return: hardcoded data
    '''
    return [['ab12','password','PHLEB'],['cd34','strongPassword','ASSIST'],['ef56','otherpassword','PATIE'],['gh78','password','PATIE']]



def handleUsersTable(connection):
    '''
    Create Tables and insert data for the users
    :param connection: connection object
    :return:
    '''
    createTable(connection, "Users",
                "(user_id int not null auto_increment , user_name varchar(20) not null, password varchar(50) not null, role varchar(20) not null, primary key(user_id))")
    data = generateDataForUsers()
    insertIntoTable(connection, "Users",
                    ["user_name", "password", "role"], data)



def main():
    connection = createConnectionObject()
    try:
        handleUsersTable(connection)
    finally:
        connection.close()



main()