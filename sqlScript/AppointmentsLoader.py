
'''
@author: Jeet Gandhi


File for loading in the appointments data
'''



from datetime import datetime, timedelta
import random
from module.DataBaseAPI import createConnectionObject,createTable,insertIntoTable,printAllValues,dropTable




def gen_datetime(min_year=2019, max_year=2020):
    # generate a datetime in format yyyy-mm-dd hh:mm:ss.000000
    start = datetime(min_year, 1, 1, 00, 00, 00)
    years = max_year - min_year + 1
    end = start + timedelta(days=365 * years)
    return start + (end - start) * random.random()


def random_with_N_digits(n):
    range_start = 10 ** (n - 1)
    range_end = (10 ** n) - 1
    return random.randint(range_start, range_end)


def generateDataForAppointments():
    """
    Function used for generating random data for appointments table
    :return: generated data to be inserted into the table
    """
    data = []
    for appointment_id in range(99, 1000):
        data.append([random_with_N_digits(2), random_with_N_digits(2), gen_datetime(), random.randrange(60)])

    return data


def handleAppointmentsTable(connection):
    """
    Complete handler for the appointments table (creates table if not present, populates it with data if necessary)
    :param connection:
    :return:
    """
    createTable(connection, "Appointments", "(appointment_id INT NOT NULL AUTO_INCREMENT primary key, "
                                            "patient_id INT NOT NULL,FOREIGN KEY patient_id(patient_id) "
                                            "REFERENCES Patients(patient_id),phleb_id INT NOT NULL, "
                                            "FOREIGN KEY phleb_id(phleb_id) REFERENCES Phlebotomist(phleb_id), "
                                            "start_time DATETIME NOT NULL, duration_mins INT NOT NULL)")
    data = generateDataForAppointments()

    insertIntoTable(connection, "Appointments", ["patient_id", "phleb_id", "start_time",
                                                 "duration_mins"], data)
    print('testing data',data)





def main():
    connection = createConnectionObject()
    try:
        handleAppointmentsTable(connection)
    finally:
        connection.close()




main()