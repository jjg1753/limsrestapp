package com.lims.limsrestapp;

public class CredentialsAndID extends Credentials {
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
