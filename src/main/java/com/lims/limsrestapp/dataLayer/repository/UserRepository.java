package com.lims.limsrestapp.dataLayer.repository;

import com.lims.limsrestapp.dataLayer.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Users, Integer> {
}
