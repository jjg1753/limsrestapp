package com.lims.limsrestapp.dataLayer.repository;


import com.lims.limsrestapp.dataLayer.model.Patients;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patients, Integer> {
}
