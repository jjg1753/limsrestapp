package com.lims.limsrestapp.dataLayer.repository;
import com.lims.limsrestapp.dataLayer.model.Assistant;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AssistantRepository extends JpaRepository<Assistant, Integer> {
}
