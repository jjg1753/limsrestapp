package com.lims.limsrestapp.dataLayer.repository;

import com.lims.limsrestapp.dataLayer.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolesRepository extends JpaRepository<Roles, Integer> {
}
