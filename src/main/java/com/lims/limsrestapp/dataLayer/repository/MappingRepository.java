package com.lims.limsrestapp.dataLayer.repository;

import com.lims.limsrestapp.dataLayer.model.Mapping;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MappingRepository extends JpaRepository<Mapping, Integer>{


}
