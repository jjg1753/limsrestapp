package com.lims.limsrestapp.dataLayer.repository;

import com.lims.limsrestapp.dataLayer.model.Phlebotomist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhlebotomistRepository extends JpaRepository<Phlebotomist, Integer> {
}
