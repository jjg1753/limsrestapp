package com.lims.limsrestapp.dataLayer.repository;

import com.lims.limsrestapp.dataLayer.model.Tests;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestsRepository extends JpaRepository<Tests, Integer> {
}
