package com.lims.limsrestapp.dataLayer.repository;


import com.lims.limsrestapp.dataLayer.model.Appointments;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppointmentRepository extends JpaRepository<Appointments, Integer> {
}
