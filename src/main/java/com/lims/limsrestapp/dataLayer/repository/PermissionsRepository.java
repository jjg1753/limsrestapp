package com.lims.limsrestapp.dataLayer.repository;

import com.lims.limsrestapp.dataLayer.model.Permissions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionsRepository extends JpaRepository<Permissions, Integer> {
}
