package com.lims.limsrestapp.dataLayer.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Mapping {
    @Id
    @GeneratedValue
    @Column(name = "lisense_number")
    String lisenseNumber;
    @Column(name="test_id")
    int testId;

    public Mapping()
    {

    }
    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public String getLisenseNumber() {
        return lisenseNumber;
    }

    public void setTestName(String lisneseNumber) {
        this.lisenseNumber = lisneseNumber;
    }
}
