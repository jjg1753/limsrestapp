package com.lims.limsrestapp.dataLayer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Disease {
    @Id
    @GeneratedValue
    @Column(name="disease_id")
    private int diseaseId;
    @Column(name="name")
    private String name;
    @Column(name="description")
    private String description;
    public void setDiseaseId(int diseaseId) {
        this.diseaseId = diseaseId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDiseaseId() {
        return diseaseId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

}
