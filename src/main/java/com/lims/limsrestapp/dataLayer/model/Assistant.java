package com.lims.limsrestapp.dataLayer.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Assistant {
    @Id
    @GeneratedValue
    @Column(name="assistant_id")
    private int assistantId;
    @Column(name="name")
    private String name;
    @Column(name="phleb_id")
    private int phlebId;

    public int getAssistantId() {
        return assistantId;
    }

    public void setAssistantId(int assistantId) {
        this.assistantId = assistantId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhlebId(int phlebId) {
        this.phlebId = phlebId;
    }

    public String getName() {
        return name;
    }

    public int getPhlebId() {
        return phlebId;
    }
}
