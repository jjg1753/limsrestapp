package com.lims.limsrestapp.dataLayer.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Patients {
    @Id
    @GeneratedValue
    @Column(name="patient_id")
    int patientId;
    @Column(name="patient_name")
    String patientName;
    @Column(name="patient_contact_number")
    String patientContactNumber;
    @Column(name="patient_SSN")
    String patientSSN;
    @Column(name="patient_age")
    String patientAge;
    @Column(name="patient_sex")
    String patientSxe;
    @Column(name="patient_address")
    String patientAddress;
    @Column(name="patient_insurance_number")
    String patientInsuranceNumber;

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public void setPatientContactNumber(String patientContactNumber) {
        this.patientContactNumber = patientContactNumber;
    }

    public void setPatientSSN(String patientSSN) {
        this.patientSSN = patientSSN;
    }

    public void setPatientAge(String patientAge) {
        this.patientAge = patientAge;
    }

    public void setPatientSxe(String patientSxe) {
        this.patientSxe = patientSxe;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public void setPatientInsuranceNumber(String patientInsuranceNumber) {
        this.patientInsuranceNumber = patientInsuranceNumber;
    }

    public int getPatientId() {
        return patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public String getPatientContactNumber() {
        return patientContactNumber;
    }

    public String getPatientSSN() {
        return patientSSN;
    }

    public String getPatientAge() {
        return patientAge;
    }

    public String getPatientSxe() {
        return patientSxe;
    }

    public String getPatientAddress() {
        return patientAddress;
    }

    public String getPatientInsuranceNumber() {
        return patientInsuranceNumber;
    }

}
