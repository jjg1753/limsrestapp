package com.lims.limsrestapp.dataLayer.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Appointments {
    @Id
    @Column(name="appointment_id")
    int appointmentId;
    @Column(name="patient_id")
    int patientID;
    @Column(name="phleb_id")
    int phleb_id;
    @Column(name="start_time")
    String startTime;
    @Column(name="duration_mins")
    int durationMins;

    public int getAppointmentId() {
        return appointmentId;
    }

    public int getPatientID() {
        return patientID;
    }

    public void setAppointmentId(int appointmentId) {
        this.appointmentId = appointmentId;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    public void setPhleb_id(int phleb_id) {
        this.phleb_id = phleb_id;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setDurationMins(int durationMins) {
        this.durationMins = durationMins;
    }

    public int getPhleb_id() {
        return phleb_id;
    }

    public String getStartTime() {
        return startTime;
    }

    public int getDurationMins() {
        return durationMins;
    }

}
