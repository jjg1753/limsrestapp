package com.lims.limsrestapp.dataLayer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Permissions {
    @Id
    @Column(name="permission_id")
    private int permissionID;
    @Column(name="task")
    private String task;

    public void setPermissionID(int permissionID) {
        this.permissionID = permissionID;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public int getPermissionID() {
        return permissionID;
    }

    public String getTask() {
        return task;
    }
}
