package com.lims.limsrestapp.dataLayer.model;


import com.lims.limsrestapp.dataLayer.model.modelHelpers.RolesKey;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@IdClass(RolesKey.class)
public class Roles {
    @Id
    @Column(name="role")
    private String role;
    @Id
    @Column(name="permission_id")
    private int permission_id;

    public void setRole(String role) {
        this.role = role;
    }

    public void setPermissionID(int permission_id) {
        this.permission_id = permission_id;
    }

    public String getRole() {
        return role;
    }

    public int getPermissionID() {
        return permission_id;
    }
}
