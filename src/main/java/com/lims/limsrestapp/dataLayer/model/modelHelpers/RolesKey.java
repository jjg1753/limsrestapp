package com.lims.limsrestapp.dataLayer.model.modelHelpers;

import java.io.Serializable;

public class RolesKey implements Serializable {
    private String role;
    private int permission_id;
}
