package com.lims.limsrestapp.dataLayer.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Phlebotomist {
    @Id
    @GeneratedValue
    @Column(name="phleb_id")
    private int phlebId;
    @Column(name="name")
    private String name;
    @Column(name="address")
    private String address;
    @Column(name="lisense_number")
    private String lisenseNumber;
    public int getPhlebId() {
        return phlebId;
    }

    public String getName() {
        return name;
    }

    public void setPhlebId(int phlebId) {
        this.phlebId = phlebId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLisenseNumber(String lisenseNumber) {
        this.lisenseNumber = lisenseNumber;
    }

    public String getAddress() {
        return address;
    }

    public String getLisenseNumber() {
        return lisenseNumber;
    }

}

