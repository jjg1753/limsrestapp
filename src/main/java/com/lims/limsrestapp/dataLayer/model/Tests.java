package com.lims.limsrestapp.dataLayer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Tests {
    @Id
    @GeneratedValue
    @Column(name="test_id")
    private int testId;
    @Column(name="test_name")
    private String testName;

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public int getTestId() {
        return testId;
    }

    public String getTestName() {
        return testName;
    }
}
