package com.lims.limsrestapp.Utility;

import com.lims.limsrestapp.dataLayer.model.Roles;

import java.util.ArrayList;
import java.util.List;

public class UtilHelpers {
    public static List<Integer> getAllPermissions(List<Roles> roles,String requestedRole)
    {
        List<Integer> allPermisibleRoles=new ArrayList<>();

        for(Roles role: roles)
        {
            if (role.getRole().equals(requestedRole)) {
                allPermisibleRoles.add(role.getPermissionID());
            }

        }

        return allPermisibleRoles;
    }
}
