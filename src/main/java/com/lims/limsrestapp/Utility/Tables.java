package com.lims.limsrestapp.Utility;

public enum Tables {
    PATIENTS("patients"),
    TESTS("tests"),
    DISEASES("diseases"),
    ASSISTANTS("assistants"),
    PHLEBOTOMISTS("phlebotomists"),
    APPOINTMENTS("appointments"),
    MAPPINGS("mappings");


    private String table;

    private Tables(String table) {
        this.table=table;
    }

    public String getTable()
    {
        return this.table;
    }
}
