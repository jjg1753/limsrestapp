package com.lims.limsrestapp.Utility;

public enum AllAssistants {
    ASSIST("ASSIST");
    private String role;

    private AllAssistants(String role) {
        this.role=role;
    }

    public String getRole()
    {
        return this.role;
    }
}
