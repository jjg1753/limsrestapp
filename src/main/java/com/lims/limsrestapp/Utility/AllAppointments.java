package com.lims.limsrestapp.Utility;

public enum AllAppointments {
    PHLEB("PHLEB"), ASSIST("ASSIST");
    private String role;

    private AllAppointments(String role) {
        this.role=role;
    }

    public String getRole()
    {
        return this.role;
    }
}
