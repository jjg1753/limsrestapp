package com.lims.limsrestapp.Utility;

import com.lims.limsrestapp.Credentials;
import com.lims.limsrestapp.dataLayer.model.Permissions;
import com.lims.limsrestapp.dataLayer.model.Roles;
import com.lims.limsrestapp.dataLayer.model.Users;
import com.lims.limsrestapp.dataLayer.repository.PermissionsRepository;
import com.lims.limsrestapp.dataLayer.repository.RolesRepository;
import com.lims.limsrestapp.dataLayer.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class Utility{
    public static String authenticate(List<Users> users, Credentials credentials)
    {
        // unpacking all the data
        List <String>allUserNames=new ArrayList<String>();
        List <String>allUserPasswords=new ArrayList<String>();
        List <String> allUserRoles=new ArrayList<String>();
        for(Users user : users)
        {
            //System.out.println(user.getUserName());
            allUserNames.add(user.getUserName());
            allUserPasswords.add(user.getPassword());
            allUserRoles.add(user.getRole());
        }
        int indexIfPresent=allUserNames.indexOf(credentials.getUser_name());
        if (indexIfPresent>=0)
        {
            // check password
            if(allUserPasswords.get(indexIfPresent).equals(credentials.getPassword()))
            {
                // user authenticated
                // return the role of the user
                return allUserRoles.get(indexIfPresent);
            }
        }
        return "UNAUTHORIZED";
    }
    public static String authorize(List<Roles> roles, List<Permissions> permissions,String requestedTable, String requestedRole)
    {
        List<Integer> permissionsIDs=UtilHelpers.getAllPermissions(roles,requestedRole);    // associated to that role, get all the permission ids
        // Using the permissions table, check if the permission exists for the requested table
        for(Permissions permission: permissions)
        {
            if (permission.getTask().equals(requestedTable))
            {
                // check if the permission id is within the allowed permission id list
                if(permissionsIDs.contains(permission.getPermissionID()))
                {
                    return requestedRole;
                }


            }
        }
        return "UNAUTHORIZED";
    }



    public static  String authenticateThenAuthorize(UserRepository userRepository, RolesRepository rolesRepository, PermissionsRepository permissionsRepository, Credentials credentials, Tables tableType)
    {

        List<Users> allUsers=userRepository.findAll();
        String response=Utility.authenticate(allUsers,credentials); // check if user is being authenticated
        if(!response.equals("UNAUTHORIZED"))
        {
            return Utility.authorize(rolesRepository.findAll(),permissionsRepository.findAll(), tableType.getTable(),response);

        }
        // check for authorization
        return "UNAUTHORIZED";
    }
}
