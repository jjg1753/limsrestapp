package com.lims.limsrestapp.Utility;

public enum AllPatients {
    PHLEB("PHLEB"), ASSIST("ASSIST");
    private String role;

    private AllPatients(String role) {
        this.role=role;
    }

    public String getRole()
    {
        return this.role;
    }
}
