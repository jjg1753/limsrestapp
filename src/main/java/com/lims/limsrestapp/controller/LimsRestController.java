package com.lims.limsrestapp.controller;

import com.lims.limsrestapp.CredentialsAndID;
import com.lims.limsrestapp.Credentials;
import com.lims.limsrestapp.Utility.*;
import com.lims.limsrestapp.dataLayer.model.*;
import com.lims.limsrestapp.dataLayer.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class LimsRestController {

    @Autowired
    MappingRepository mappingRepository;
    @Autowired
    AssistantRepository assistantRepository;
    @Autowired
    DiseaseRepository diseaseRepository;
    @Autowired
    PhlebotomistRepository phlebotomistRepository;
    @Autowired
    TestsRepository testsRepository;
    @Autowired
    AppointmentRepository appointmentRepository;
    @Autowired
    PatientRepository patientRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PermissionsRepository permissionsRepository;
    @Autowired
    RolesRepository rolesRepository;

    @RequestMapping("/welcome")
    public String welcomeToLims() {
        return "Welcome to Appointments Management System";
    }


    @RequestMapping("/getAllPhlebotomists")
    public List<Phlebotomist> getAllPhlebotomists(@RequestBody Credentials credentials) {
        String response=Utility.authenticateThenAuthorize(userRepository,rolesRepository, permissionsRepository, credentials,Tables.PHLEBOTOMISTS); // check if user is being authenticated
        if(response.equals("UNAUTHORIZED"))
        {
            return new ArrayList<>();
        }
        return phlebotomistRepository.findAll();
    }
    @RequestMapping("/getAllAssistants")
    public List<Assistant> getAllAssistants(@RequestBody Credentials credentials) {
        String response=Utility.authenticateThenAuthorize(userRepository,rolesRepository, permissionsRepository, credentials,Tables.ASSISTANTS); // check if user is being authenticated
        if(response.equals("UNAUTHORIZED"))
        {
            return new ArrayList<>();
        }
        // check role again to identify data breach
        for(AllAssistants role:AllAssistants.values())
        {
            if(role.name().equals(response))
            {
                return assistantRepository.findAll();
            }
        }
        return new ArrayList<>();
    }
    @RequestMapping("/getAssistant")
    public List<Assistant> getAllAssistants(@RequestBody CredentialsAndID credentials) {
        String response=Utility.authenticateThenAuthorize(userRepository,rolesRepository, permissionsRepository, credentials,Tables.ASSISTANTS); // check if user is being authenticated
        if(response.equals("UNAUTHORIZED"))
        {
            return new ArrayList<>();
        }
        List<Assistant> payload=new ArrayList<>();
        payload.add(assistantRepository.findById(credentials.getId()).get());
        return payload;
    }

    @RequestMapping("/getAllDiseases")
    public List<Disease> getAllDiseases(@RequestBody Credentials credentials) {
        // allowed roles: phlebotomist
        String response=Utility.authenticateThenAuthorize(userRepository,rolesRepository, permissionsRepository, credentials,Tables.DISEASES); // check if user is being authenticated
        if(response.equals("UNAUTHORIZED"))
        {
            return new ArrayList<>();
        }
        return diseaseRepository.findAll();

    }
    @RequestMapping("/getAllTests")
    public List<Tests> getAllTests(@RequestBody Credentials credentials) {
        // allowed roles: phlebotomist
        String response=Utility.authenticateThenAuthorize(userRepository,rolesRepository, permissionsRepository, credentials,Tables.TESTS); // check if user is being authenticated
        if(response.equals("UNAUTHORIZED"))
        {
            return new ArrayList<>();
        }
        return testsRepository.findAll();
    }
    @RequestMapping("/getAllPatients")
    public List<Patients> getAllPatients(@RequestBody Credentials credentials) {
        // get all the users in the table
        String response=Utility.authenticateThenAuthorize(userRepository,rolesRepository, permissionsRepository, credentials,Tables.PATIENTS); // check if user is being authenticated
        if(response.equals("UNAUTHORIZED"))
        {
            return new ArrayList<>();
        }
        // check role again to identify data breach
        for(AllPatients role:AllPatients.values())
        {
            if(role.name().equals(response))
            {
                return patientRepository.findAll();
            }
        }
        return new ArrayList<>();     // User completely authorized
    }
    @RequestMapping("/getPatient")
    public List<Patients> getPatients(@RequestBody CredentialsAndID credentials) {
        // get all the users in the table
        String response=Utility.authenticateThenAuthorize(userRepository,rolesRepository, permissionsRepository, credentials,Tables.PATIENTS); // check if user is being authenticated
        if(response.equals("UNAUTHORIZED"))
        {
            return new ArrayList<>();
        }
        List<Patients> payload=new ArrayList<>();
        payload.add(patientRepository.findById(credentials.getId()).get());
        return payload;
    }
    @RequestMapping("/getAllAppointments")
    public List<Appointments> getAllAppointmentss(@RequestBody Credentials credentials) {
        // accessible to only the phlebotomists and the assistants
        String response=Utility.authenticateThenAuthorize(userRepository,rolesRepository, permissionsRepository, credentials,Tables.APPOINTMENTS); // check if user is being authenticated
        if(response.equals("UNAUTHORIZED"))
        {
            return new ArrayList<>();
        }
        // check role again to identify data breach
        for(AllAppointments role:AllAppointments.values())
        {
            if(role.name().equals(response))
            {
                return appointmentRepository.findAll();
            }
        }
        return new ArrayList<>();
    }
    @RequestMapping("/getAppointment")
    public List<Appointments> getAppointment(@RequestBody CredentialsAndID credentials) {
        // access granted based on appointmentId
        // Patient, Assistant and Phlebotomist are allowed
        String response=Utility.authenticateThenAuthorize(userRepository,rolesRepository, permissionsRepository, credentials,Tables.APPOINTMENTS); // check if user is being authenticated
        if(response.equals("UNAUTHORIZED"))
        {
            return new ArrayList<>();
        }
        List<Appointments> payload=new ArrayList<>();
        payload.add(appointmentRepository.findById(credentials.getId()).get());
        return payload;
    }

}
